# Java Experiment

This project builds and tests a Java-based API, then deploys it to Elastic Beanstalk. It includes a simple smoke test and some unit tests.
